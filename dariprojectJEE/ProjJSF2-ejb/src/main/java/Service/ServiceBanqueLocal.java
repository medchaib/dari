package Service;

import java.util.List;

import javax.ejb.Local;

import entity.Banque;



@Local
public interface ServiceBanqueLocal {
	 public void addCategorie(Banque c);
	 public void removeCategorie(int id);
		
	 public void updateCategorie(Banque c);
	 public List<Banque> getAllcategorie();
}

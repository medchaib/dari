package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entity.*;

/**
 * Session Bean implementation class ServiceCategorie
 */
@Stateless
@LocalBean
public class ServiceBanque implements ServiceBanqueRemote, ServiceBanqueLocal {
	@PersistenceContext
	EntityManager em ;	
    /**
     * Default constructor. 
     */
    public ServiceBanque() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addCategorie(Banque c) {
		em.persist(c);
		
	}

	@Override
	public void removeCategorie(int id) {
		em.remove(em.find(Banque.class , id));
		
	}

	@Override
	public void updateCategorie(Banque c) {
		em.merge(c);
		
	}

	@Override
	public List<Banque> getAllcategorie() {
		TypedQuery<Banque> query =
				em.createQuery("select e from Banque e",
						Banque.class);
		return query.getResultList();
	}
	

}

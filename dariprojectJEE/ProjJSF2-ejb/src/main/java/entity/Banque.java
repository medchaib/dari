package entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Banque implements Serializable{
	@Id
	@GeneratedValue 
	  public int id;
      public String libelle ;
      public String adresse ;
      public double interet ;
      
      
      
	public Banque(int id, String libelle, String adresse, double interet) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.adresse = adresse;
		this.interet = interet;
	}
	public Banque() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public double getInteret() {
		return interet;
	}
	public void setInteret(double interet) {
		this.interet = interet;
	}
      
      
}

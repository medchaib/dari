package Ressources;

import javax.ejb.EJB;

import javax.faces.bean.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.ws.rs.core.Response.Status;

import entity.*;
import Service.*;
import java.security.Provider.Service;

@Path("competance")
@RequestScoped
public class BanqueRessource {
	@EJB
	ServiceBanqueLocal cm;

	public BanqueRessource() {
		super();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response AllBanque() {
		return Response.ok().entity(cm. getAllcategorie()).build();
	}
	
	
	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteBanque(@PathParam("id") int id) {
	
		    cm.removeCategorie(id);
			return Response.status(Response.Status.OK).entity("la suppression est effectuée").build();
		
	}
	
	
	
	@POST
	@Path("new")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addBanque (Banque e) throws Exception{
		//return Response.status(Status.OK).entity(cm.addCategorie(e)).build();
	
		return null;
	}
	
	
}
